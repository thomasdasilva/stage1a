<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210624071357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE message_admin_utilisateur DROP FOREIGN KEY FK_4E33682B6D0D4601');
        $this->addSql('DROP TABLE message_admin');
        $this->addSql('DROP TABLE message_admin_utilisateur');
        $this->addSql('DROP TABLE message_utilisateur');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE message_admin (id INT AUTO_INCREMENT NOT NULL, expediteur_id INT NOT NULL, titre VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, date_envoie DATETIME NOT NULL, INDEX IDX_77A8F29D10335F61 (expediteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE message_admin_utilisateur (message_admin_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_4E33682B6D0D4601 (message_admin_id), INDEX IDX_4E33682BFB88E14F (utilisateur_id), PRIMARY KEY(message_admin_id, utilisateur_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE message_utilisateur (message_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_7D633D4A537A1329 (message_id), INDEX IDX_7D633D4AFB88E14F (utilisateur_id), PRIMARY KEY(message_id, utilisateur_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE message_admin ADD CONSTRAINT FK_77A8F29D10335F61 FOREIGN KEY (expediteur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE message_admin_utilisateur ADD CONSTRAINT FK_4E33682B6D0D4601 FOREIGN KEY (message_admin_id) REFERENCES message_admin (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message_admin_utilisateur ADD CONSTRAINT FK_4E33682BFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
    }
}
