<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210528140624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE demande ADD id_demandeur_id INT DEFAULT NULL, ADD id_cariste_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A5B6679B2B FOREIGN KEY (id_demandeur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A5D38F7F9D FOREIGN KEY (id_cariste_id) REFERENCES utilisateur (id)');
        $this->addSql('CREATE INDEX IDX_2694D7A5B6679B2B ON demande (id_demandeur_id)');
        $this->addSql('CREATE INDEX IDX_2694D7A5D38F7F9D ON demande (id_cariste_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE demande DROP FOREIGN KEY FK_2694D7A5B6679B2B');
        $this->addSql('ALTER TABLE demande DROP FOREIGN KEY FK_2694D7A5D38F7F9D');
        $this->addSql('DROP INDEX IDX_2694D7A5B6679B2B ON demande');
        $this->addSql('DROP INDEX IDX_2694D7A5D38F7F9D ON demande');
        $this->addSql('ALTER TABLE demande DROP id_demandeur_id, DROP id_cariste_id');
    }
}
