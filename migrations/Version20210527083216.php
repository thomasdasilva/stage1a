<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527083216 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article ADD code VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE demande DROP FOREIGN KEY FK_2694D7A55A3A7B7A');
        $this->addSql('DROP INDEX IDX_2694D7A55A3A7B7A ON demande');
        $this->addSql('ALTER TABLE demande ADD article_id INT DEFAULT NULL, DROP code_article_id, CHANGE entrepot_source_id entrepot_source_id INT DEFAULT NULL, CHANGE entrepot_dest_id entrepot_dest_id INT DEFAULT NULL, CHANGE unite_id unite_id INT DEFAULT NULL, CHANGE quantite quantite INT NOT NULL');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A57294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_2694D7A57294869C ON demande (article_id)');
        $this->addSql('ALTER TABLE utilisateur CHANGE profile_id profile_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP code');
        $this->addSql('ALTER TABLE demande DROP FOREIGN KEY FK_2694D7A57294869C');
        $this->addSql('DROP INDEX IDX_2694D7A57294869C ON demande');
        $this->addSql('ALTER TABLE demande ADD code_article_id INT NOT NULL, DROP article_id, CHANGE entrepot_source_id entrepot_source_id INT NOT NULL, CHANGE entrepot_dest_id entrepot_dest_id INT NOT NULL, CHANGE unite_id unite_id INT NOT NULL, CHANGE quantite quantite DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE demande ADD CONSTRAINT FK_2694D7A55A3A7B7A FOREIGN KEY (code_article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_2694D7A55A3A7B7A ON demande (code_article_id)');
        $this->addSql('ALTER TABLE utilisateur CHANGE profile_id profile_id INT NOT NULL');
    }
}
