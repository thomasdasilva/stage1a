<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210610115305 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX code ON article');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E6677153098 ON article (code)');
        $this->addSql('ALTER TABLE demande ADD date_validation DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur CHANGE login login VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_23a0e6677153098 ON article');
        $this->addSql('CREATE UNIQUE INDEX code ON article (code)');
        $this->addSql('ALTER TABLE demande DROP date_validation');
        $this->addSql('ALTER TABLE utilisateur CHANGE login login VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
