<?php

namespace App\Repository;

use App\Entity\Demande;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Demande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Demande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Demande[]    findAll()
 * @method Demande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Demande::class);
    }
    
    /**
     * Retourne le jeu des enregistrements des demandes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandes(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_creation','DESC')
            ->addOrderBy('date_besoin','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes valider
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesI(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.Etat = 1')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes non valider
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesO(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.Etat = 0')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de validation les plus récentes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateVal(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_valide','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de validation les plus récentes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateValDESC(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_valide','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de besoin les plus récentes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateBesoinDESC(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_besoin','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de besoin les plus anciennes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateBesoin(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_besoin','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de création les plus anciennes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateCreation(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_creation','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes trier par
     * date de création les plus récentes
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesOrderDateCreationDESC(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('date_creation','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesDemandeur($idUser): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('dem.id = :idUser')
            ->setParameter('idUser', $idUser)
            ->orderBy('date_creation','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes où il n'y a pas de cariste
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesCaristeNull(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('car.id IS NULL')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du cariste
     * 
     * @param int $idCariste Identifiant du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesCariste(int $idCariste): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin >= CURRENT_DATE()')
            ->andwhere('car.id = :idUser')
            ->orwhere('(d.Etat=0')
            ->andwhere('car.id = :idUser)')
            ->setParameter('idUser', $idCariste)
            ->orderBy('d.dateBesoin','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du cariste du jour
     * 
     * @param int $idCariste Identifiant du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesCaristeuUrgent(int $idCariste): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('(car.id = :idUser')
            ->andwhere('d.dateBesoin = CURRENT_DATE())')
            ->orwhere('(d.Etat = 0')
            ->andwhere('car.id = :idUser)')
            ->setParameter('idUser', $idCariste)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du cariste du jour
     * 
     * @param int $idCariste Identifiant du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesRetard(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin < CURRENT_DATE()')
            ->andwhere('d.Etat = 0')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du cariste trier par etat et la date du jour 
     * 
     * @param int $idCariste Identifiant du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesCaristeOrderByEtat(int $idCariste): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin >= CURRENT_DATE()')
            ->andwhere('car.id = :idUser')
            ->setParameter('idUser', $idCariste)
            ->orderBy('d.Etat','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes du cariste trier par etat et la date du jour 
     * 
     * @param int $idCariste Identifiant du demandeur
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesCaristeOrderByEtatDESC(int $idCariste): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin >= CURRENT_DATE()')
            ->andwhere('car.id = :idUser')
            ->setParameter('idUser', $idCariste)
            ->orderBy('d.Etat','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes avec un article 
     * 
     * @param int $idArticle Identifiant de l'article
     * 
     * @return array id sous forme de tableau
     */
    public function findAllDemandesWithArticle($idArticle): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('d.id')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('a.id = :id')
            ->setParameter('id', $idArticle)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes avec une unité 
     * 
     * @param int $id Identifiant de l'unité
     * 
     * @return array id sous forme de tableau
     */
    public function findAllDemandesWithUnite($id): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('d.id')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes avec un entrepot 
     * 
     * @param int $id Identifiant de l'entrepot
     * 
     * @return array id sous forme de tableau
     */
    public function findAllDemandesWithEntrepot($id): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('d.id')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->where('z1.id = :id')
            ->orWhere('z2.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes avec un 
     * utilisateur 
     * 
     * @param int $id Identifiant de l'utilisateur
     * 
     * @return array id sous forme de tableau
     */
    public function findAllDemandesWithUtilisateur($id): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('d.id')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->where('dem.id = :id')
            ->orWhere('car.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des demandes filtré
     * 
     * @param string $libelle le libelle de l'entrepot source
     * 
     * @return array id, date de création, date de besoin, quantite, entrepot source, entrepot destination, article, unité, nom du demandeur, nom du cariste et etat sous forme de tableau
     */
    public function findAllDemandesFiltre($etat, $libelleEntrepotS, $libelleEntrepotE, $libelleArticle, $nomDemandeur, $nomCariste): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->add('select','d.id')
            ->addSelect('d.Etat AS etat')
            ->addSelect('d.dateValidation AS date_valide')
            ->addSelect('d.dateCreation AS date_creation')
            ->addSelect('d.dateBesoin AS date_besoin')
            ->addSelect('d.quantite AS quantite')
            ->addSelect('z1.libelle AS entrepot_source_libelle')
            ->addSelect('z2.libelle AS entrepot_dest_libelle')
            ->addSelect('a.libelle AS article_libelle ')
            ->addSelect('u.libelle AS unite_libelle  ')
            ->addSelect('dem.nom AS nom_demandeur  ')
            ->addSelect('car.nom AS nom_cariste ')
            ->add('from', 'App\Entity\Demande d')
            ->leftJoin('App\Entity\Zone','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('App\Entity\Zone','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('App\Entity\Article','a','WITH', 'a.id = d.article')
            ->leftJoin('App\Entity\Unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('App\Entity\Utilisateur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('App\Entity\Utilisateur','car','WITH', 'car.id= d.idCariste')
            ->orderBy('etat','ASC');

            if ($etat !== "" and $etat !== null) {
                $qb->where('d.Etat = '.$etat);
            }
            if ($libelleEntrepotS !== "" and $libelleEntrepotS !== null) {
                $qb->andwhere('z1.libelle = \''.$libelleEntrepotS.'\'');
            }
            if ($libelleEntrepotE !== "" and $libelleEntrepotE !== null) {
                $qb->andwhere('z2.libelle = \''.$libelleEntrepotE.'\'');
            }
            if ($libelleArticle !== "" and $libelleArticle !== null) {
                $qb->andwhere('a.libelle = \''.$libelleArticle.'\'');
            }
            if ($nomDemandeur !== "" and $nomDemandeur !== null) {
                 $qb->andwhere('dem.nom = \''.$nomDemandeur.'\'');
            }
            
            if ($nomCariste == "0") {
                $qb->andwhere('car.nom IS NULL');
            }
            if ($nomCariste !== null and $nomCariste!== "" and $nomCariste!== "0" ) {
                $qb->andwhere('car.nom = \''.$nomCariste.'\''); 
            }
            
        return $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * Retourne le jeu des enregistrements des demandes entre 2 dates
     * 
     * @param int $id Identifiant de l'utilisateur
     * 
     * @return array id sous forme de tableau
     */
    public function findAllDemandesBetweenDate($date1,$date2): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('d')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('d.entrepotSource','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('d.entrepotDest','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('d.article','a','WITH', 'a.id = d.article')
            ->leftJoin('d.unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('d.idDemandeur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('d.idCariste','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin BETWEEN :date1 AND :date2')
            ->setParameter('date1', $date1->format)
            ->setParameter('date2',$date2->format)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le nombre de demandes entre 2 dates
     * 
     * @param int $id Identifiant de l'utilisateur
     * 
     * @return array id sous forme de tableau
     */
    public function nbDemandeMois($date1,$date2): int
    {
        $qb = $this->createQueryBuilder()
            ->select('count(d)')
            ->from('App\Entity\Demande', 'd')
            ->leftJoin('d.entrepotSource','z1','WITH', 'z1.id = d.entrepotSource')
            ->leftJoin('d.entrepotDest','z2','WITH', 'z2.id = d.entrepotDest')
            ->leftJoin('d.article','a','WITH', 'a.id = d.article')
            ->leftJoin('d.unite','u','WITH', 'u.id = d.unite')
            ->leftJoin('d.idDemandeur','dem','WITH', 'dem.id= d.idDemandeur')
            ->leftJoin('d.idCariste','car','WITH', 'car.id= d.idCariste')
            ->where('d.dateBesoin >= :date1')
            ->andWhere('d.dateBesoin <= :date2')
            ->setParameter('date1', $date1->format)
            ->setParameter('date2', $date2->format)
            ->getQuery()
            ->getResult();
            //dump($qb[0][1]);
        return $qb[0][1];
    }

    // /**
    //  * @return Demande[] Returns an array of Demande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Demande
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
