<?php

namespace App\Repository;

use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof Utilisateur) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loadUserByUsername(string $login): ?User
    {
        $entityManager = $this->getEntityManager();

        return $entityManager->createQuery(
                'SELECT u
                FROM App\Entity\Utilisateur u
                WHERE u.login = :query'
            )
            ->setParameter('query', $login)
            ->getOneOrNullResult();
    }

    /**
     * Retourne le jeu des enregistrements des utilisateurs caristes
     * 
     * @return array id et login des utilisateurs sous forme de tableau
     */
    public function findAllCariste(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('u.id')
            ->addSelect('u.login AS login_Cariste')
            ->from('App\Entity\Utilisateur', 'u')
            ->where('u.idProfile = 3')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des utilisateurs caristes
     * 
     * @return array id et login des utilisateurs sous forme de tableau
     */
    public function findAllDestinataire(): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.idProfile=3')
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * Retourne le jeu des enregistrements des utilisateurs
     * 
     * @return array id, login, nom, prenom, profile des utilisateurs sous forme de tableau
     */
    public function findAllUtilisateur(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('u.id')
            ->addSelect('u.login AS login')
            ->addSelect('u.nom AS nom, u.prenom')
            ->addSelect('u.prenom AS prenom')
            ->addSelect('u.email AS mail')
            ->addSelect('IDENTITY(u.idProfile) AS profile')
            ->from('App\Entity\Utilisateur', 'u')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des utilisateurs trier par
     * profile
     * 
     * @return array id, login, nom, prenom, profile des utilisateurs sous forme de tableau
     */
    public function findAllUtilisateurOrderByProfile(): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('u.id')
            ->addSelect('u.login AS login')
            ->addSelect('u.nom AS nom, u.prenom')
            ->addSelect('u.prenom AS prenom')
            ->addSelect('u.email AS mail')
            ->addSelect('IDENTITY(u.idProfile) AS profile')
            ->from('App\Entity\Utilisateur', 'u')
            ->orderBy('IDENTITY(u.idProfile)','ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    /**
     * Retourne le jeu des enregistrements des utilisateurs
     * 
     * @param int $idUtilisateur Identifiant de l'utilisateur
     * 
     * @return array id du profile des utilisateurs sous forme de tableau
     */
    public function findIdProfile(int $idUtilisateur): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('u.id')
            ->addSelect('p.id AS idprofile')
            ->addSelect('p.libelle AS libelle')
            ->from('App\Entity\Utilisateur', 'u')
            ->join('App\Entity\Profile', 'p', 'WITH', 'p.id = u.idProfile')
            ->where('u.id = :idutilisateur')
            ->setParameter('idutilisateur',$idUtilisateur)
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        return $qb;
    }

    // /**
    //  * @return Utilisateur[] Returns an array of Utilisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Utilisateur
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
