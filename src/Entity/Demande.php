<?php

namespace App\Entity;

use App\Repository\DemandeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemandeRepository::class)
 */
class Demande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Zone::class)
     */
    private $entrepotSource;

    /**
     * @ORM\ManyToOne(targetEntity=Zone::class)
     */
    private $entrepotDest;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class)
     */
    private $article;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Unite::class)
     */
    private $unite;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="date")
     */
    private $dateBesoin;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class)
     */
    private $idDemandeur;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class)
     */
    private $idCariste;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Etat;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateValidation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntrepotSource(): ?Zone
    {
        return $this->entrepotSource;
    }

    public function setEntrepotSource(?Zone $entrepotSource): self
    {
        $this->entrepotSource = $entrepotSource;

        return $this;
    }

    public function getEntrepotDest(): ?Zone
    {
        return $this->entrepotDest;
    }

    public function setEntrepotDest(?Zone $entrepotDest): self
    {
        $this->entrepotDest = $entrepotDest;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateBesoin(): ?\DateTimeInterface
    {
        return $this->dateBesoin;
    }

    public function setDateBesoin(\DateTimeInterface $dateBesoin): self
    {
        $this->dateBesoin = $dateBesoin;

        return $this;
    }

    public function getIdDemandeur(): ?Utilisateur
    {
        return $this->idDemandeur;
    }

    public function setIdDemandeur(?Utilisateur $idDemandeur): self
    {
        $this->idDemandeur = $idDemandeur;

        return $this;
    }

    public function getIdCariste(): ?Utilisateur
    {
        return $this->idCariste;
    }

    public function setIdCariste(?Utilisateur $idCariste): self
    {
        $this->idCariste = $idCariste;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->Etat;
    }

    public function setEtat(bool $Etat): self
    {
        $this->Etat = $Etat;

        return $this;
    }

    public function getDateValidation(): ?\DateTimeInterface
    {
        return $this->dateValidation;
    }

    public function setDateValidation(?\DateTimeInterface $dateValidation): self
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }
}
