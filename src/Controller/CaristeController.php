<?php

namespace App\Controller;

use App\Entity\Demande;
use App\Entity\Article;
use App\Entity\Unite;
use App\Entity\Zone;
use App\Entity\Utilisateur;
use App\Entity\Profile;
use App\Form\CreationDemandeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Require ROLE_CARISTE for *every* controller method in this class.
 *
 * @IsGranted("ROLE_CARISTE")
 */
class CaristeController extends AbstractController
{
    /**
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste", name="cariste")
     */  
    public function caristeDashboard(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_CARISTE', null, 'User tried to access a page without having ROLE_CARISTE');
        return $this->render('cariste/caristePage.html.twig', [
            'controller_name' => 'CaristeController',
        ]);
    }

    /**
     * Controlleur pour la liste des demandes du cariste
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheListeDemandeCariste", name="afficheListeDemandeCariste")
     */
    public function afficheListeDemandeCariste(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $donnees = $repository->findAllDemandesCariste($user->getid());

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }
        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('cariste/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))
        );
    }

    /**
     * Controlleur pour la liste des demandes trié par etat 1
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheListeDemandeCaristeOrderByEtat", name="afficheListeDemandeCaristeOrderByEtat")
     */
    public function afficheListeDemandeCaristeOrderByEtat(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $donnees = $repository->findAllDemandesCaristeOrderByEtatDESC($user->getid());

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('cariste/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7),
                'filtre' => 1 )

        );
    }

    /**
     * Controlleur pour la liste des demandes trié par etat 0
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheListeDemandeCaristeOrderByEtat0", name="afficheListeDemandeCaristeOrderByEtat0")
     */
    public function afficheListeDemandeCaristeOrderByEtat0(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $donnees = $repository->findAllDemandesCaristeOrderByEtat($user->getid());

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('cariste/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7),
                'filtre' => 1)

        );
    }

    /**
     * Controlleur pour la liste des demandes du Cariste à réaliser pour la date d'aujoud'hui
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheListeDemandeCaristeUrgent", name="afficheListeDemandeCaristeUrgent")
     */
    public function afficheListeDemandeCaristeUrgent(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $donnees = $repository->findAllDemandesCaristeuUrgent($user->getid());

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('cariste/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7),
                'filtre' => 1)

        );
    }

    /**
     * Controlleur pour la validation des demandes du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/validerDemandeCariste/{idDemande}", name="validerDemandeCariste")
     */
    public function validerDemandeCariste(int $idDemande, Request $request):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Demande::class);
        $demande = $repository->find($idDemande);

        if ($demande) {
            $demande->setEtat(1);
            $demande->setDateValidation(new \DateTime());
            $entityManager->flush();
            $this->addFlash('success', "l'état de la demande ".$idDemande." a bien été validé");
            return $this->redirect($this->generateUrl("afficheListeDemandeCariste"));
        }
        $this->addFlash('erreur', "pas de demande pour l'id :".$idDemande);
        return $this->redirect($this->generateUrl("afficheListeDemandeCariste"));
    }

    /**
     * Controlleur pour le profile du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheProfileCariste", name="afficheProfileCariste")
     */
    public function afficheProfileCariste(): Response
    {
        $user = $this->getUser();
        return $this->render('cariste/afficheProfile.html.twig', ['utilisateur'=> $user]);
    }

    /**
     * Controlleur pour la modification du profile du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/modificationProfileCariste", name="modificationProfileCariste")
     */  
    public function modificationProfileCariste(Request $request): Response
    {   
        $utilisateur = $this->getUser();
        $idUtilisateur = $utilisateur->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifUtilisateur(
                $utilisateur->getLogin(),
                $request->request->get("form")['login'],
                $request->request->get("form")['nom'],
                $request->request->get("form")['prenom'],
                $request->request->get("form")['mail'],
                $utilisateur->getEmail()
            )) {
                $login = $request->request->get("form")['login'];
                $nom = $request->request->get("form")['nom'];
                $prenom = $request->request->get("form")['prenom'];
                $utilisateur->setLogin($login);
                $utilisateur->setNom($nom);
                $utilisateur->setPrenom($prenom);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'votre profile a bien été modifié');
                return $this->redirect($this->generateUrl("afficheProfileCariste"));
            }
        }
        return $this->render("cariste/modificationProfile.html.twig", [
            'utilisateur' => $utilisateur
        ]);
    }

    /**
     * Controlleur pour la modification du mot de passe du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/modificationPasswordCariste", name="modificationPasswordCariste")
     */  
    public function modificationPasswordCariste(UserPasswordEncoderInterface $encoder,Request $request): Response
    {   
        $utilisateur = $this->getUser();
        $idUtilisateur = $utilisateur->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifPassword(
                $utilisateur->getLogin(),
                $request->request->get("form")['motdepasse'],
                $request->request->get("form")['oldmotdepasse'],
                $request->request->get("form")['repeatmotdepasse']
            )) {
                $password = $request->request->get("form")['motdepasse'];
                $encoded = $encoder->encodePassword($utilisateur, $password);
                $utilisateur->setPassword($encoded);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'votre mot de passe a bien été modifié');
                return $this->redirect($this->generateUrl("afficheProfileCariste"));
            }
        }
        return $this->render("cariste/modificationPassword.html.twig", [
            'utilisateur' => $utilisateur
        ]);
    }

    /**
     * Controlleur pour la vérification de la modification du mot de passe du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     */
    public function verificationModifPassword(String $login, String $password, String $oldpassword, String $repeatpassword): bool
    {
        $estValide = true;
        if (!password_verify($oldpassword,$this->getUser()->getPassword())) {
            $this->addFlash('erreur', "ancien mot de passe incorrect");
            $estValide = false;
        }      
        if (strlen($password) > 255 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        if ($password !== $repeatpassword) {
            $this->addFlash('erreur', "les nouveaux mots de passe sont differents");
            $estValide = false;
        }
        return $estValide;
    }


    /**
     * Controlleur pour la vérification de la modification du profile du Cariste 
     *
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     */
    public function verificationModifUtilisateur(String $oldLogin, String $login, String $nom, String $prenom, String $mail, String $oldMail): bool
    {
        $estValide = true;
        if ($login == "" or $nom == "" or $prenom == ""or $mail == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }

        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $listeUtilisateur = $repository->findAllUtilisateur();

        if (!$listeUtilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur'
            );
        }
        $c=0;
        $c2=0;
        for ($i=0; $i<count($listeUtilisateur) ; $i++) { 
            if (strcmp($login,$listeUtilisateur[$i]['login']) == 0) {
                $c = $c+1;
            }
            if (strcmp($mail,$listeUtilisateur[$i]['mail']) == 0) {
                $c2= $c2+1;
            }
        }
        if ($login !== $oldLogin) {
            if ($c !== 0) {
                $this->addFlash('erreur', "le login existe déjà");
            $estValide = false;
            }
        }
        if ($mail !== $oldMail) {
            if ($c2 !== 0) {
                $this->addFlash('erreur', "l'adresse mail existe déjà");
            $estValide = false;
            }
        }
        
        if (strlen($login) >50 or strlen($nom) > 255 or strlen($prenom) > 255 or strlen($mail) > 255) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    public function index(): Response
    {
        return $this->render('cariste/index.html.twig', [
            'controller_name' => 'CaristeController',
        ]);
    }
}