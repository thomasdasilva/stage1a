<?php

namespace App\Controller;

use App\Entity\Demande;
use App\Entity\Article;
use App\Entity\Unite;
use App\Entity\Zone;
use App\Entity\Utilisateur;
use App\Entity\Profile;
use App\Form\CreationEntrepotType;
use App\Form\CreationUniteType;
use App\Form\CreationArticleType;
use App\Form\AjouterCaristeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin", name="admin")
     */    
    public function adminDashboard(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');
        return $this->render('admin/adminPage.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
    /**
     * Controlleur pour la liste des demandes
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemande", name="affichelisteDemande")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemande(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandes();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }
        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur pour les statistiques
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheStatistique", name="afficheStatistique")
     * @return Response Objet contenant le template
     */
    public function afficheStatistique(Request $request):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $filtre = array();
        if ($request->query->get('article')!==null) {
            $filtre += [ "etat"=> $request->query->get('etat') ];
            $filtre += [ "entrepotS"=> $request->query->get('entrepotS') ];
            $filtre += [ "entrepotD"=> $request->query->get('entrepotD') ];
            $filtre += [ "article"=> $request->query->get('article') ];
            $filtre += [ "demandeur"=> $request->query->get('demandeur') ];
            $filtre += [ "cariste"=> $request->query->get('cariste') ];
            $donnees = $repository->findAllDemandesFiltre($filtre["etat"], $filtre["entrepotS"], $filtre["entrepotD"], $filtre["article"], $filtre["demandeur"], $filtre["cariste"]);
        }
        else{
            for ($i=0; $i <=5 ; $i++) { 
                array_push($filtre, null);
            }
            $donnees = $repository->findAllDemandesFiltre($filtre[0], $filtre[1], $filtre[2], $filtre[3], $filtre[4], $filtre[5]);
        }

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        //dump($filtre);die();

        $nbDemande = 0;
        $nbRetard = 0;
        $nbAlHeure = 0;
        $nbDemandeValider = 0;
        
        $nbDemandeAn = array_fill(0, 12, 0);
        $nbRetardAn = array_fill(0, 12, 0);
        $nbAlHeureAn = array_fill(0, 12, 0);
        $nbDemandeAnValider = array_fill(0, 12, 0);
        
        if ($request->query->get("annee") !== null) {
            $annee = intval($request->query->get("annee"));
            $dateA = new DateTime(date("Y"));
            $anneeActuelle = $dateA->format;
            $moisAnnee = 1;
        }
        if ($request->query->get("mois") !== null) {
            $moisAnnee = intval($request->query->get("mois"));
            $dateM = new DateTime(date("m"));
        }
        
        /**else{
            $dateA = new DateTime(date("Y"));
            $annee = $dateA->format;
            $anneeActuelle = $annee;
            //$date2 = new DateTime(date("Y-m-01", strtotime('+1 month')));
            //$date2 = new DateTime(date("Y-01-01", strtotime('+1 year')));
        }
        */

        if ($moisAnnee == 1 OR $moisAnnee == 3 OR $moisAnnee == 5 OR $moisAnnee == 7 OR $moisAnnee == 8 OR $moisAnnee == 10 OR $moisAnnee == 12 ) {
            $cal = range(1,31);
            $nbDemandeMois = array_fill(0, 31, 0);
            $nbRetardMois = array_fill(0, 31, 0);
            $nbAlHeureMois = array_fill(0, 31, 0);
            $nbDemandeMoisValider = array_fill(0, 31, 0);
        }
        elseif ($moisAnnee == 2) {
            if ($annee % 4 == 0) {
                $cal = range(1,29);
                $nbDemandeMois = array_fill(0, 29, 0);
                $nbRetardMois = array_fill(0, 29, 0);
                $nbAlHeureMois = array_fill(0, 29, 0);
                $nbDemandeMoisValider = array_fill(0, 29, 0);
            }else{
                $cal = range(1,28);
                $nbDemandeMois = array_fill(0, 28, 0);
                $nbRetardMois = array_fill(0, 28, 0);
                $nbAlHeureMois = array_fill(0, 28, 0);
                $nbDemandeMoisValider = array_fill(0, 28, 0);
            }
        }
        else{
            $cal = range(1,30);
            $nbDemandeMois = array_fill(0, 30, 0);
            $nbRetardMois = array_fill(0, 30, 0);
            $nbAlHeureMois = array_fill(0, 30, 0);
            $nbDemandeMoisValider = array_fill(0, 30, 0);
        }

        for ($i=0; $i < count($donnees) ; $i++) {

            // si la date de la demande correspond à l'année

            if (date_format($donnees[$i]['date_creation'],"Y") == $annee ) {
                $nbDemande++;
                $mois = date_format($donnees[$i]['date_creation'],"m")-1;
                $nbDemandeAn[$mois] ++;
                // si en retard 
                if ($donnees[$i]['date_valide']>$donnees[$i]['date_besoin'] AND $donnees[$i]['date_valide'] !== null) {
                    $nbRetard = $nbRetard+1;
                    $nbRetardAn[$mois] ++;
                    $nbDemandeValider ++;
                    $nbDemandeAnValider[$mois] ++;
                }
                elseif ($donnees[$i]['date_valide']<=$donnees[$i]['date_besoin'] AND $donnees[$i]['date_valide'] !== null) {
                    $nbAlHeure ++;
                    $nbAlHeureAn[$mois] ++;
                    $nbDemandeValider ++;
                    $nbDemandeAnValider[$mois] ++;
                }
            }

            // si la date de la demande correspond à l'année et au mois
            if (date_format($donnees[$i]['date_creation'],"Y") == $annee AND date_format($donnees[$i]['date_creation'],"m") == $moisAnnee){
                
                $jour = date_format($donnees[$i]['date_creation'],"j")-1;
                $nbDemandeMois[$jour] ++;
                // si en retard
                if ($donnees[$i]['date_valide']>$donnees[$i]['date_besoin'] AND $donnees[$i]['date_valide'] !== null){
                    
                    $nbRetardMois[$mois] ++;
                    
                    $nbDemandeMoisValider[$mois] ++;
                }
                elseif ($donnees[$i]['date_valide']<=$donnees[$i]['date_besoin'] AND $donnees[$i]['date_valide'] !== null) {
                    
                    $nbAlHeureMois[$mois] ++;
                    
                    $nbDemandeMoisValider[$mois] ++;
                }
            }
        }
        $moyDemandeMois = array_sum ( $nbDemandeAn )/12;

        return $this->render('admin/afficheStatistique.html.twig', array(
                'donnees' => $donnees,
                'nbDemande' => $nbDemande,
                'nbRetard' => $nbRetard,
                'nbAlHeure' => $nbAlHeure,
                'nbDemandeValider' => $nbDemandeValider,
                'nbDemandeMois' => $nbDemandeMois,
                'nbRetardMois' => $nbRetardMois,
                'nbAlHeureMois' => $nbAlHeureMois,
                'nbDemandeMoisValider' => $nbDemandeMoisValider,
                'moyDemandeMois' => $moyDemandeMois,
                'nbDemandeAn' => $nbDemandeAn,
                'nbDemandeAnValider' => $nbDemandeAnValider,
                'nbRetardAn' => $nbRetardAn,
                'nbAlHeureAn' => $nbAlHeureAn,
                'annee' => $annee,
                'mois' => $moisAnnee,
                'cal' => $cal,
                'filtre' => $filtre
            )
        );
    }

    /**
     * Controlleur pour les statistiques
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheGraphStatistique", name="afficheGraphStatistique")
     * @return Response Objet contenant le template
     */
    public function afficheGraphStatistique(Request $request):Response
    {
        //dump($request);die();
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $filtre = array();
        if ($request->query->get('article')!==null) {
            $filtre += [ "etat"=> $request->query->get('etat') ];
            $filtre += [ "entrepotS"=> $request->query->get('entrepotS') ];
            $filtre += [ "entrepotD"=> $request->query->get('entrepotD') ];
            $filtre += [ "article"=> $request->query->get('article') ];
            $filtre += [ "demandeur"=> $request->query->get('demandeur') ];
            $filtre += [ "cariste"=> $request->query->get('cariste') ];
            $donnees = $repository->findAllDemandesFiltre($filtre["etat"], $filtre["entrepotS"], $filtre["entrepotD"], $filtre["article"], $filtre["demandeur"], $filtre["cariste"]);
        }
        else{
            for ($i=0; $i <=5 ; $i++) { 
                array_push($filtre, null);
            }
            $donnees = $repository->findAllDemandesFiltre($filtre[0], $filtre[1], $filtre[2], $filtre[3], $filtre[4], $filtre[5]);
        }

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        // variable avec les informations sur l'année
        $nbDemande = 0;
        $nbRetard = 0;
        $nbAlHeure = 0;
        $nbDemandeValider = 0;

        // variable avec les informations sur le mois
        $nbDemandeAn = array_fill(0, 12, 0);
        $nbRetardAn = array_fill(0, 12, 0);
        $nbAlHeureAn = array_fill(0, 12, 0);
        $nbDemandeAnValider = array_fill(0, 12, 0);
        
        if ($request->query->get("annee") !== null) {
            $annee = intval($request->query->get("annee"));
            $dateA = new DateTime(date("Y"));
            $anneeActuelle = $dateA->format;
            $moisAnnee = -1;
        }
        if ($request->query->get("mois") !== null) {
            $moisAnnee = intval($request->query->get("mois"));
            $dateM = new DateTime(date("m"));
        }
        
        /**else{
            $dateA = new DateTime(date("Y"));
            $annee = $dateA->format;
            $anneeActuelle = $annee;
            //$date2 = new DateTime(date("Y-m-01", strtotime('+1 month')));
            //$date2 = new DateTime(date("Y-01-01", strtotime('+1 year')));
        }
        */

        if ($moisAnnee == 1 OR $moisAnnee == 3 OR $moisAnnee == 5 OR $moisAnnee == 7 OR $moisAnnee == 8 OR $moisAnnee == 10 OR $moisAnnee == 12 ) {
            $cal = range(1,31);
            $nbDemandeMois = array_fill(0, 31, 0);
            $nbRetardMois = array_fill(0, 31, 0);
            $nbAlHeureMois = array_fill(0, 31, 0);
            $nbDemandeMoisValider = array_fill(0, 31, 0);
        }
        elseif ($moisAnnee == 2) {
            if ($annee % 4 == 0) {
                $cal = range(1,29);
                $nbDemandeMois = array_fill(0, 29, 0);
                $nbRetardMois = array_fill(0, 29, 0);
                $nbAlHeureMois = array_fill(0, 29, 0);
                $nbDemandeMoisValider = array_fill(0, 29, 0);
            }else{
                $cal = range(1,28);
                $nbDemandeMois = array_fill(0, 28, 0);
                $nbRetardMois = array_fill(0, 28, 0);
                $nbAlHeureMois = array_fill(0, 28, 0);
                $nbDemandeMoisValider = array_fill(0, 28, 0);
            }
        }
        else{
            $cal = range(1,30);
            $nbDemandeMois = array_fill(0, 30, 0);
            $nbRetardMois = array_fill(0, 30, 0);
            $nbAlHeureMois = array_fill(0, 30, 0);
            $nbDemandeMoisValider = array_fill(0, 30, 0);
        }

        for ($i=0; $i < count($donnees) ; $i++) {

            // si la date de la demande correspond à l'année

            if (date_format($donnees[$i]['date_creation'],"Y") == $annee ) {
                $nbDemande++;
                $mois = date_format($donnees[$i]['date_creation'],"m")-1;
                $nbDemandeAn[$mois] ++;
            }
            if ($donnees[$i]['date_valide'] !== null) {
                if (date_format($donnees[$i]['date_valide'],"Y") == $annee ) {    
                    // si en retard 
                    if ($donnees[$i]['date_valide']>$donnees[$i]['date_besoin']) {
                        $nbRetard = $nbRetard+1;
                        $nbRetardAn[date_format($donnees[$i]['date_valide'],"m")-1] ++;
                        $nbDemandeValider ++;
                        $nbDemandeAnValider[date_format($donnees[$i]['date_valide'],"m")-1] ++;
                    }
                    elseif ($donnees[$i]['date_valide']<=$donnees[$i]['date_besoin']) {
                        $nbAlHeure ++;
                        $nbAlHeureAn[date_format($donnees[$i]['date_valide'],"m")-1] ++;
                        $nbDemandeValider ++;
                        $nbDemandeAnValider[date_format($donnees[$i]['date_valide'],"m")-1] ++;
                    }
                }
            }

            // si la date de la demande correspond à l'année et au mois
            if (date_format($donnees[$i]['date_creation'],"Y") == $annee AND date_format($donnees[$i]['date_creation'],"m") == $moisAnnee){
                $jour = date_format($donnees[$i]['date_creation'],"j")-1;
                $nbDemandeMois[$jour] ++;
            }
            if ($donnees[$i]['date_valide'] !== null) {
                if (date_format($donnees[$i]['date_valide'],"Y") == $annee AND date_format($donnees[$i]['date_valide'],"m") == $moisAnnee){
                    // si en retard
                    if ($donnees[$i]['date_valide']>$donnees[$i]['date_besoin']){
                        $nbRetardMois[date_format($donnees[$i]['date_valide'],"j")-1] ++;
                        $nbDemandeMoisValider[date_format($donnees[$i]['date_valide'],"j")-1] ++;
                    }
                    elseif ($donnees[$i]['date_valide']<=$donnees[$i]['date_besoin']) {
                        $nbAlHeureMois[date_format($donnees[$i]['date_valide'],"j")-1] ++;
                        $nbDemandeMoisValider[date_format($donnees[$i]['date_valide'],"j")-1] ++;
                    }
                }
            }    
        }
        $moyDemandeMois = array_sum ( $nbDemandeAn )/12;
        return $this->render('admin/afficheGraphStatistique.html.twig', array(
                'donnees' => $donnees,
                'nbDemande' => $nbDemande,
                'nbRetard' => $nbRetard,
                'nbAlHeure' => $nbAlHeure,
                'nbDemandeValider' => $nbDemandeValider,
                'nbDemandeMois' => json_encode($nbDemandeMois),
                'nbRetardMois' => json_encode($nbRetardMois),
                'nbAlHeureMois' => json_encode($nbAlHeureMois),
                'nbDemandeMoisValider' => json_encode($nbDemandeMoisValider),
                'moyDemandeMois' => $moyDemandeMois,
                'nbDemandeAn' => json_encode($nbDemandeAn),
                'nbDemandeAnValider' => json_encode($nbDemandeAnValider),
                'nbRetardAn' => json_encode($nbRetardAn),
                'nbAlHeureAn' => json_encode($nbAlHeureAn),
                'annee' => $annee,
                'mois' => $moisAnnee,
                'cal' => json_encode($cal),
                'filtre' => $filtre
            )
        );
    }


    /**
     * Controlleur de la liste des demande trié par Etat
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeI", name="affichelisteDemandeI")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeI(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesI();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->request->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par Etat négatif
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeO", name="affichelisteDemandeO")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeO(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesO();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de validation
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateVal", name="affichelisteDemandeOrderDateVal")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateVal(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateVal();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de validation
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateValDESC", name="affichelisteDemandeOrderDateValDESC")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateValDESC(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateValDESC();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de besoin
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateBesoinDESC", name="affichelisteDemandeOrderDateBesoinDESC")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateBesoinDESC(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateBesoinDESC();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de besoin
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateCreation", name="affichelisteDemandeOrderDateCreation")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateCreation(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateCreation();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de besoin
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateCreationDESC", name="affichelisteDemandeOrderDateCreationDESC")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateCreationDESC(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateCreationDESC();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande trié par date de besoin
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeOrderDateBesoin", name="affichelisteDemandeOrderDateBesoin")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeOrderDateBesoin(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesOrderDateBesoin();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande en retard
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeRetard", name="affichelisteDemandeRetard")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeRetard(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $donnees = $repository->findAllDemandesRetard();

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur de la liste des demande filtré
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/affichelisteDemandeFiltre", name="affichelisteDemandeFiltre")
     * @return Response Objet contenant le template
     */
    public function affichelisteDemandeFiltre(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $filtre = array();
        if ($request->query->get('article')!==null) {
            $filtre += [ "etat"=> $request->query->get('etat') ];
            $filtre += [ "entrepotS"=> $request->query->get('entrepotS') ];
            $filtre += [ "entrepotD"=> $request->query->get('entrepotD') ];
            $filtre += [ "article"=> $request->query->get('article') ];
            $filtre += [ "demandeur"=> $request->query->get('demandeur') ];
            $filtre += [ "cariste"=> $request->query->get('cariste') ];
            $donnees = $repository->findAllDemandesFiltre($filtre["etat"], $filtre["entrepotS"], $filtre["entrepotD"], $filtre["article"], $filtre["demandeur"], $filtre["cariste"]);
        }
        else{
            for ($i=0; $i <=5 ; $i++) { 
                array_push($filtre, null);
            }
            $donnees = $repository->findAllDemandesFiltre($filtre[0], $filtre[1], $filtre[2], $filtre[3], $filtre[4], $filtre[5]);
        }

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }
        
        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page',1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );
        return $this->render('admin/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7),
                'filtre' => $filtre
            )
        );
    }

    /**
     * Controlleur de la liste des Articles
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeArticle", name="afficheListeArticle")
     * @return Response Objet contenant le template
     */
    public function afficheListeArticle(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Article::class);
        $donnees = $repository->findAllArticle();

        if (!$donnees) {
            $this->addFlash('pb', "pas d'article");
        }

        $listeArticle = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page',1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeArticle.html.twig',
            array(
                'listeArticle' => $listeArticle,
                'nbPages' => ceil($listeArticle->getTotalItemCount()/7)
            )
        );
    }

    /**
     * Controlleur de la liste des unités
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeUnite", name="afficheListeUnite")
     */
    public function afficheListeUnite(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Unite::class);
        $donnees = $repository->findAllUnite();

        if (!$donnees) {
            $this->addFlash('pb', "pas d'unité");
        }

        $listeUnite = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page',1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeUnite.html.twig',
            array(
                'listeUnite' => $listeUnite,
                'nbPages' => ceil($listeUnite->getTotalItemCount()/7)
            )
        );
    }

    /**
     * Controlleur de la liste des entrepots
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeEntrepot", name="afficheListeEntrepot")
     */
    public function afficheListeEntrepot(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Zone::class);
        $donnees = $repository->findAllEntrepot();

        if (!$donnees) {
            $this->addFlash('pb', "pas d'entrepot");
        }
        $listeEntrepot = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeEntrepot.html.twig',
            array('listeEntrepot' => $listeEntrepot,
                'nbPages' => ceil($listeEntrepot->getTotalItemCount()/7))
        );
    }

    /**
     * Controlleur pour la liste des utilisateurs
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeUtilisateur", name="afficheListeUtilisateur")
     */
    public function afficheListeUtilisateur(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $donnees = $repository->findAllUtilisateur();

        if (!$donnees) {
            $this->addFlash('pb', "pas d'utilisateur");
        }

        $listeUtilisateur = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeUtilisateur.html.twig',
            array('listeUtilisateur' => $listeUtilisateur,
            'nbPages' => ceil($listeUtilisateur->getTotalItemCount()/7))
        );
    }

    /**
     * Controlleur pour la liste des utilisateurs trié par profile
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeUtilisateurOrderByProfile", name="afficheListeUtilisateurOrderByProfile")
     */
    public function afficheListeUtilisateurOrderByProfile(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $donnees = $repository->findAllUtilisateurOrderByProfile();

        if (!$donnees) {
            $this->addFlash('pb', "pas d'utilisateur");
        }

        $listeUtilisateur = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('admin/afficheListeUtilisateur.html.twig',
            array('listeUtilisateur' => $listeUtilisateur,
                'nbPages' => ceil($listeUtilisateur->getTotalItemCount()/7))
        );
    }

    /**
     * Controlleur de création d'article
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/creationArticle", name="creationArticle")
     */
    public function creationArticle(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $article = new Article();

        if ($request->isMethod('POST')) {
            if ($this->verificationCreationArticle(
                $request->request->get("form")['code'],
                $request->request->get("form")['libelle'])
            ){
                $code = $request->request->get("form")['code'];
                $article->setCode($code);

                $libelle = $request->request->get("form")['libelle'];
                $article->setLibelle($libelle);

                $entityManager->persist($article);
                $entityManager->flush();
                $this->addFlash('success', 'l\'article a bien été créé');
                return $this->redirect($this->generateUrl("afficheListeArticle", ["page" => "1"]));
            }
        }

        return $this->render('admin/creationArticle.html.twig');
    }

    /**
     * Controlleur de modification d'article
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/modificationArticle/{idArticle}", name="modificationArticle")
     */
    public function modificationArticle(int $idArticle,Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Article::class);
        $article = $repository->find($idArticle);

        if (!$article) {
            throw $this->createNotFoundException(
                'pas d\'article pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifArticle(
                $article->getCode(),
                $request->request->get("form")['code'],
                $request->request->get("form")['libelle'])
            ){
                $code = $request->request->get("form")['code'];
                $article->setCode($code);

                $libelle = $request->request->get("form")['libelle'];
                $article->setLibelle($libelle);

                $entityManager->flush();
                $this->addFlash('success', 'l\'article a bien été modifié');
                return $this->redirect($this->generateUrl("afficheListeArticle", ["page" => "1"]));
            }
        }

        return $this->render("admin/modificationArticle.html.twig", [
            'article' => $article]);
    }
    /**
     * Controlleur de suppression d'article
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/suppressionArticle/{idArticle}", name="suppressionArticle")
     */
    public function suppressionArticle(int $idArticle,Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Article::class);
        $article = $repository->find($idArticle);

        if ($article) {
            if ($this->verificationSupprArticle($idArticle)) {
                $entityManager->remove($article);
                $entityManager->flush();
                $this->addFlash('success', 'l\'article a bien été supprimé');
                return $this->redirect($this->generateUrl("afficheListeArticle", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeArticle", ['page' => '1']));
        }
        $this->addFlash('erreur', 'pas d\'article');
        return $this->redirect($this->generateUrl("afficheListeArticle", ['page' => '1']));
    }

    /**
     * Controlleur de création d'unité
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/creationUnite", name="creationUnite")
     */
    public function creationUnite(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $unite = new Unite();

        if ($request->isMethod('POST')) {
            if ($this->verificationLibelle(
                $request->request->get("form")['libelle'])
            ){
                $libelle = $request->request->get("form")['libelle'];
                $unite->setLibelle($libelle);

                $entityManager->persist($unite);
                $entityManager->flush();
                $this->addFlash('success', 'l\'unité a bien été créé');
                return $this->redirect($this->generateUrl("afficheListeUnite", ['page' => '1']));
            }
        }

        return $this->render('admin/creationUnite.html.twig');
    }

    /**
     * Controlleur de modification d'unité
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/modificationUnite/{idUnite}", name="modificationUnite")
     */
    public function modificationUnite(int $idUnite,Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Unite::class);
        $unite = $repository->find($idUnite);

        if (!$unite) {
            throw $this->createNotFoundException(
                'pas d\'unite pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationLibelle(
                $request->request->get("form")['libelle'])
            ){
                $libelle = $request->request->get("form")['libelle'];
                $unite->setLibelle($libelle);

                $entityManager->flush();
                $this->addFlash('success', 'l\'unité a bien été modifié');
                return $this->redirect($this->generateUrl("afficheListeUnite", ['page' => '1']));
            }
        }

        return $this->render("admin/modificationUnite.html.twig", [
            'unite' => $unite]);
    }

    /**
     * Controlleur de suppression de suppression d'unité
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/suppressionUnite/{idUnite}", name="suppressionUnite")
     */
    public function suppressionUnite(int $idUnite, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Unite::class);
        $unite = $repository->find($idUnite);

        if ($unite) {
            if ($this->verificationSupprUnite($idUnite)) {
                $entityManager->remove($unite);
                $entityManager->flush();
                $this->addFlash('success', 'l\'unité a bien été supprimé');
                return $this->redirect($this->generateUrl("afficheListeUnite", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeUnite", ['page' => '1']));
        }
        $this->addFlash('erreur', 'pas d\'unité');
        return $this->redirect($this->generateUrl("afficheListeUnite", ['page' => '1']));
    }

    /**
     * Controlleur de création d'entrepot
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/creationEntrepot", name="creationEntrepot")
     */
    public function creationEntrepot(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entrepot = new Zone();

        if ($request->isMethod('POST')) {
            if ($this->verificationLibelle(
                $request->request->get("form")['libelle'])
            ){

                $libelle = $request->request->get("form")['libelle'];
                $entrepot->setLibelle($libelle);

                $entityManager->persist($entrepot);
                $entityManager->flush();
                $this->addFlash('success', 'l\'entrepot a bien été créé');
                return $this->redirect($this->generateUrl("afficheListeEntrepot", ['page' => '1']));
            }
        }

        return $this->render('admin/creationEntrepot.html.twig');
    }

    /**
     * Controlleur de modification d'entrepot
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/modificationEntrepot/{idEntrepot}", name="modificationEntrepot")
     */
    public function modificationEntrepot(int $idEntrepot,Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Zone::class);
        $entrepot = $repository->find($idEntrepot);

        if (!$entrepot) {
            throw $this->createNotFoundException(
                'pas d\'entrepot pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationLibelle(
                $request->request->get("form")['libelle'])
            ){
                $libelle = $request->request->get("form")['libelle'];
                $entrepot->setLibelle($libelle);
                $entityManager->flush();
                $this->addFlash('success', 'l\'entrepot a bien été modifié');
                return $this->redirect($this->generateUrl("afficheListeEntrepot", ['page' => '1']));
            }
        }

        return $this->render("admin/modificationEntrepot.html.twig", [
            'entrepot' => $entrepot]);
    }

    /**
     * Controlleur de suppression d'entrepot
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/suppressionEntrepot/{idEntrepot}", name="suppressionEntrepot")
     */
    public function suppressionEntrepot(int $idEntrepot, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Zone::class);
        $entrepot = $repository->find($idEntrepot);

        if ($entrepot) {
            if ($this->verificationSupprEntrepot($idEntrepot)) {
                $entityManager->remove($entrepot);
                $entityManager->flush();
                $this->addFlash('success', 'l\'entrepot a bien été supprimé');
                return $this->redirect($this->generateUrl("afficheListeEntrepot", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeEntrepot", ['page' => '1']));
        }
        $this->addFlash('erreur', 'pas d\'entrepot');
        return $this->redirect($this->generateUrl("afficheListeEntrepot", ['page' => '1']));
    }

    /**
     * Controlleur du formulaire d'ajout d'un cariste à une demande
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/formulaireCaristeDemande/{idDemande}", name="formulaireCaristeDemande")
     */
    public function formulaireCaristeDemande(int $idDemande, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Utilisateur::class);
        $listeCariste = $repository->findAllCariste();
        if (!$listeCariste) {
            throw $this->createNotFoundException(
                'pas de cariste'
            );
        }

        return $this->render('admin/formulaireCaristeDemande.html.twig', [
            'idDemande' => $idDemande,
            'listeCariste' => $listeCariste
        ]);
    }

    /**
     * Controlleur d'ajout d'un cariste à une demande
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/ajouterCaristeDemande/{idDemande}", name="ajouterCaristeDemande")
     */
    public function ajouterCaristeDemande(int $idDemande, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $demande = $entityManager->getRepository(Demande::class)->find($idDemande);

        if (!$demande) {
            throw $this->createNotFoundException(
                'pas de demande'
            );
        }
        if ($request->isMethod('POST')) {
            if ($this->verificationCaristeExiste(
                $request->request->get('cariste'))
            ){
                $cariste = $entityManager->getRepository(Utilisateur::class)->find($request->request->get('cariste'));
                $demande->setIdCariste($cariste);
                $entityManager->flush();
                $this->addFlash('success', 'le carriste a bien été affecté');
                return $this->redirect($this->generateUrl("affichelisteDemande", ['page' => '1']));
            }
        }
        return $this->redirect($this->generateUrl("formulaireCaristeDemande", ['page' => '1']));
    }

    /**
     * Controlleur du formulaire d'ajout d'un cariste à plusieurs demandes
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/formulaireMultipleCaristeDemande", name="formulaireMultipleCaristeDemande")
     */
    public function formulaireMultipleCaristeDemande(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryC=  $entityManager->getRepository(Utilisateur::class);
        $listeCariste = $repositoryC->findAllCariste();
        if (!$listeCariste) {
            throw $this->createNotFoundException(
                'pas de cariste'
            );
        }
        $repositoryD=  $entityManager->getRepository(Demande::class);
        $listeDemande = $repositoryD->findAllDemandesCaristeNull();
        if (!$listeDemande) {
            throw $this->createNotFoundException(
                'pas de demande'
            );
        }

        return $this->render('admin/formulaireMultipleCaristeDemande.html.twig', [
            'listeCariste' => $listeCariste,
            'listeDemande' => $listeDemande
        ]);
    }

    /**
     * Controlleur d'ajout d'un cariste à plusieurs demandes
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/ajoutMultipleCaristeDemande", name="ajoutMultipleCaristeDemande")
     */
    public function ajoutMultipleCaristeDemande(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $listeDemande = $request->request->get('demandeId');
        if ($listeDemande) {
            if ($request->isMethod('POST')) {
                if ($this->verificationCaristeExiste($request->request->get('cariste'))){
                    $cariste = $entityManager->getRepository(Utilisateur::class)->find($request->request->get('cariste'));
                    for ($i=0; $i < count($listeDemande) ; $i++) {
                        
                        $demande = $entityManager->getRepository(Demande::class)->find($listeDemande[$i]);

                        if (!$demande) {
                            throw $this->createNotFoundException(
                                'pas de demande'
                            );
                        }
                        $demande->setIdCariste($cariste);
                        $entityManager->flush();
                    }
                    $this->addFlash('success', 'le carriste a bien été affecté à toutes les demandes selectionnées');
                    return $this->redirect($this->generateUrl("affichelisteDemande", ['page' => '1']));
                }
            }
        }
        if (!$listeDemande) {
            $this->addFlash('erreur', 'aucune demande selectionnées');
        }
        return $this->redirect($this->generateUrl("formulaireMultipleCaristeDemande"));
    }

    /**
     * Controlleur du formulaire de création d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/formulaireCreationUtilisateur", name="formulaireCreationUtilisateur")
     */
    public function formulaireCreationUtilisateur(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Profile::class);
        $listeProfile = $repository->findAllProfile();
        if (!$listeProfile) {
            throw $this->createNotFoundException(
                'pas de profile '
            );
        }

        return $this->render('admin/CreationUtilisateur.html.twig', [
            'listeProfile' => $listeProfile
        ]);
    }

    /**
     * Controlleur de création d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/CreationUtilisateur", name="CreationUtilisateur")
     */
    public function CreationUtilisateur(UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $utilisateur = new Utilisateur();
        if ($request->isMethod('POST')) {
            if ($this->verificationCreationUtilisateur(
                $request->request->get("form")['login'],
                $request->request->get("form")['nom'],
                $request->request->get("form")['prenom'],
                $request->request->get("form")['mail'],
                $request->request->get("form")['motdepasse'],
                $request->request->get("form")['profile']
            )) {
                $login =$request->request->get("form")['login'];
                $nom = $request->request->get("form")['nom'];
                $prenom = $request->request->get("form")['prenom'];
                $mail = $request->request->get("form")['mail'];
                $password = $request->request->get("form")['motdepasse'];
                $profile = $entityManager->getRepository(Profile::class)->find($request->request->get("form")['profile']);
                $utilisateur->setLogin($login);
                $utilisateur->setNom($nom);
                $utilisateur->setPrenom($prenom);
                $utilisateur->setEmail($mail);
                $encoded = $encoder->encodePassword($utilisateur, $password);
                $utilisateur->setPassword($encoded);
                $utilisateur->setIdProfile($profile);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'l\'utilisateur a bien été créé');
                return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
            }
        }

        return $this->redirect($this->generateUrl("formulaireCreationUtilisateur"));
    }

    /**
     * Controlleur de modification d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/modificationUtilisateur/{idUtilisateur}", name="modificationUtilisateur")
     */
    public function modificationUtilisateur(int $idUtilisateur, UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);
        $profileUsers = $repositoryU->findIdProfile($idUtilisateur);
        $profileUser = $profileUsers[0];
        $repositoryP=  $entityManager->getRepository(Profile::class);
        $listeProfile = $repositoryP->findAllProfile();

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if (!$listeProfile) {
            throw $this->createNotFoundException(
                'pas de profile'
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifUtilisateur(
                $utilisateur->getLogin(),
                $request->request->get("form")['login'],
                $request->request->get("form")['nom'],
                $request->request->get("form")['prenom'],
                $utilisateur->getEmail(),
                $request->request->get("form")['mail'],
                $request->request->get("form")['profile']
            )) {
                $login = $request->request->get("form")['login'];
                $nom = $request->request->get("form")['nom'];
                $prenom = $request->request->get("form")['prenom'];
                $mail = $request->request->get("form")['mail'];
                $profile = $entityManager->getRepository(Profile::class)->find($request->request->get("form")['profile']);
                $utilisateur->setLogin($login);
                $utilisateur->setNom($nom);
                $utilisateur->setPrenom($prenom);
                $utilisateur->setEmail($mail);
                $utilisateur->setIdProfile($profile);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'l\'utilisateur a bien été modifié');
                return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
        }
        return $this->render("admin/modificationUtilisateur.html.twig", [
            'idUtilisateur' => $idUtilisateur,
            'profileUser' => $profileUser,
            'utilisateur' => $utilisateur,
            'listeProfile' => $listeProfile
        ]);
    }

    /**
     * Controlleur de modification du mot de passe d'un utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/modificationPasswordUtilisateur/{idUtilisateur}", name="modificationPasswordUtilisateur")
     */
    public function modificationPasswordUtilisateur(int $idUtilisateur, UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifPassword(
                $utilisateur->getLogin(),
                $request->request->get("form")['motdepasse']
            )) {                
                $password = $request->request->get("form")['motdepasse'];
                $encoded = $encoder->encodePassword($utilisateur, $password);
                $utilisateur->setPassword($encoded);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'le mot de passe de l\'utilisateur a bien été modifié');
                return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
        }
        return $this->render("admin/modificationPasswordUtilisateur.html.twig", [
            'idUtilisateur' => $idUtilisateur,
            'utilisateur' => $utilisateur
        ]);
    }

    /**
     * Controlleur de suppression d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/suppressionUtilisateur/{idUtilisateur}", name="suppressionUtilisateur")
     */
    public function suppressionUtilisateur(int $idUtilisateur, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repository->find($idUtilisateur);

        if ($utilisateur) {
            if ($this->verificationSupprUtilisateur($idUtilisateur)) {
                $entityManager->remove($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'l\'utilisateur a bien été supprimé');
                return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
            }
            return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
        }
        $this->addFlash('erreur', 'pas d\'utilisateur');
        return $this->redirect($this->generateUrl("afficheListeUtilisateur", ['page' => '1']));
    }

    /**
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationCaristeExiste(String $idCariste): bool
    {
        $estValide = true;
        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $cariste = $repository->find($idCariste);

        if ($idCariste == ""){
            $this->addFlash('erreur', "Sélectionner un cariste");
            return false;
        }
        elseif (!$cariste) {
            $this->addFlash('erreur', "le cariste n'existe pas");
            $estValide = false;
        }
        return $estValide;
    }


    /**
     * Controlleur de verification de la création d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationCreationUtilisateur(String $login, String $nom, String $prenom, String $mail, String $motdepasse, String $idProfile): bool
    {
        $estValide = true;
        if ($login == "" or $nom == "" or $prenom == "" or $mail == "" or $motdepasse == "" or $idProfile == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }
        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $listeUtilisateur = $repository->findAllUtilisateur();

        if (!$listeUtilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur'
            );
        }
        $c=0;
        $c2=0;
        for ($i=0; $i<count($listeUtilisateur) ; $i++) { 
            if (strcmp($login,$listeUtilisateur[$i]['login']) == 0) {
                $c = $c+1;
            }
            if (strcmp($mail,$listeUtilisateur[$i]['mail']) == 0) {
                $c2++;
            }
        }
        if ($c !== 0) {
            $this->addFlash('erreur', "le login existe déjà");
            $estValide = false;
        }

        if ($c2 !== 0) {
            $this->addFlash('erreur', "l'adresse mail existe déjà");
            $estValide = false;
        }

        if ($idProfile !=="1" and $idProfile !=="2" and $idProfile !=="3" ) {
            $this->addFlash('erreur', "le profile n'existe pas");
            $estValide = false;
        }

        if (strlen($login) > 50 or strlen($nom) > 255 or strlen($prenom) > 255 or strlen($mail) > 255 or strlen($motdepasse) > 255 or strlen($idProfile) > 1 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la modification d'utilisateur
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationModifUtilisateur(String $oldLogin, String $login, String $nom, String $prenom, String $oldMail, String $mail, int $idProfile): bool
    {
        $estValide = true;
        if ($login == "" or $nom == "" or $prenom == "" or $mail == "" or $idProfile == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }

        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $listeUtilisateur = $repository->findAllUtilisateur();

        if (!$listeUtilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur'
            );
        }
        $c=0;
        $c2=0;
        for ($i=0; $i<count($listeUtilisateur) ; $i++) { 
            if (strcmp($login,$listeUtilisateur[$i]['login']) == 0) {
                $c = $c+1;
            }
            if (strcmp($mail,$listeUtilisateur[$i]['mail']) == 0) {
                $c2= $c2+1;
            }
        }
        if ($login !== $oldLogin) {
            if ($c !== 0) {
                $this->addFlash('erreur', "le login existe déjà");
            $estValide = false;
            }
        }
        if ($mail !== $oldMail) {
            if ($c2 !== 0) {
                $this->addFlash('erreur', "l'adresse mail existe déjà");
            $estValide = false;
            }
        }
        
        if (strlen($login) >50 or strlen($nom) > 255 or strlen($prenom) > 255 or strlen($mail) > 255  or strlen($idProfile) > 1 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la modification du mot de passe de l'utilisateur
     * 
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     */
    public function verificationModifPassword(String $login, String $password): bool
    {
        $estValide = true;       
        if (strlen($password) > 255 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la création d'article
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationCreationArticle(String $code, String $libelle): bool
    {
        $estValide = true;
        if ($code == "" or $libelle == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }
        $repository= $this->getDoctrine()->getRepository(Article::class);
        $listeArticle = $repository->findAllArticle();

        if (!$listeArticle) {
            throw $this->createNotFoundException(
                'pas d\'article'
            );
        }
        $c=0;
        for ($i=0; $i<count($listeArticle) ; $i++) {
            if (strcmp($code,$listeArticle[$i]['code_article']) == 0) {
                $c = $c+1;
            }
        }
        
        if ($c !== 0) {
            $this->addFlash('erreur', "le code existe déjà");
            $estValide = false;
        }

        if (strlen($code) > 50 or strlen($libelle) > 255) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la modification d'article
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationModifArticle(String $oldCode, String $code, String $libelle): bool
    {
        $estValide = true;
        if ($code == "" or $libelle == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }

        $repository= $this->getDoctrine()->getRepository(Article::class);
        $listeArticle = $repository->findAllArticle();

        if (!$listeArticle) {
            throw $this->createNotFoundException(
                'pas d\'article'
            );
        }
        $c=0;
        for ($i=0; $i<count($listeArticle) ; $i++) {
            if (strcmp($code,$listeArticle[$i]['code_article']) == 0) {
                $c = $c+1;
            }
        }
        if ($code !== $oldCode) {
            if ($c !== 0) {
                $this->addFlash('erreur', "le code existe déjà");
            $estValide = false;
            }
        }

        if (strlen($code) > 50 or strlen($libelle) > 255) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la suppression d'article
     *
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationSupprArticle(String $idArticle): bool
    {
        $estValide = true;
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $listeDemande = $repository->findAllDemandesWithArticle($idArticle);

        if ($listeDemande) {
            $this->addFlash('erreur', "l'article est utilisé dans une demande");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification du libelle d'un entrepot ou d'une unité
     *
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationLibelle(String $libelle): bool
    {
        $estValide = true;
        if ($libelle == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }
        if (strlen($libelle) > 255) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la suppression d'une unité
     *
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationSupprUnite(String $id): bool
    {
        $estValide = true;
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $listeDemande = $repository->findAllDemandesWithUnite($id);

        if ($listeDemande) {
            $this->addFlash('erreur', "l'unité est utilisé dans une demande");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la suppression d'une entrepot
     *
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationSupprEntrepot(String $id): bool
    {
        $estValide = true;
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $listeDemande = $repository->findAllDemandesWithEntrepot($id);

        if ($listeDemande) {
            $this->addFlash('erreur', "l'entrepot est utilisé dans une demande");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur de verification de la suppression d'un utilisateur
     *
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     */
    public function verificationSupprUtilisateur(String $id): bool
    {
        $estValide = true;
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $listeDemande = $repository->findAllDemandesWithUtilisateur($id);

        if ($listeDemande) {
            $this->addFlash('erreur', "l'utilisateur est utilisé dans une demande");
            $estValide = false;
        }
        if ($this->getUser()->getId()==$id) {
            $this->addFlash('erreur', "Vous ne pouvez pas vous supprimer");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur 
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/index", name="index")
     * @return Response Objet contenant le template
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
}
