<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class SecurityController extends AbstractController
{
     /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {   
        if ($this->getUser()) {
            if ($this->getUser()->getRoles()[0]=='ROLE_ADMIN') {
                return $this->redirect($this->generateUrl("admin"));
            } elseif ($this->getUser()->getRoles()[0]=='ROLE_DEMANDEUR') {
                return $this->redirect($this->generateUrl("demandeur"));
            } elseif ($this->getUser()->getRoles()[0]=='ROLE_CARISTE') {
                return $this->redirect($this->generateUrl("cariste"));
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/log", name="connecter")
     */
    public function log(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            if ($this->getUser()->getRoles()[0]=='ROLE_ADMIN') {
                return $this->redirect($this->generateUrl("admin"));
            } elseif ($this->getUser()->getRoles()[0]=='ROLE_DEMANDEUR') {
                return $this->redirect($this->generateUrl("demandeur"));
            } elseif ($this->getUser()->getRoles()[0]=='ROLE_CARISTE') {
                return $this->redirect($this->generateUrl("cariste"));
            } 
        } return $this->redirect($this->generateUrl("app_logout"));
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): Response
    {
        return $this->redirect($this->generateUrl("app_login"));
    }
   
    public function index(): Response
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    } 
}
