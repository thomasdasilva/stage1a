<?php

namespace App\Controller;

use App\Entity\Demande;
use App\Entity\Article;
use App\Entity\Unite;
use App\Entity\Zone;
use App\Entity\Utilisateur;
use App\Entity\Profile;
use App\Form\CreationDemandeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Require ROLE_DEMANDEUR for *every* controller method in this class.
 *
 * @IsGranted("ROLE_DEMANDEUR")
 */
class DemandeurController extends AbstractController
{
    /**
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem", name="demandeur")
     */  
    public function demDashboard(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_DEMANDEUR', null, 'User tried to access a page without having ROLE_DEMANDEUR');
        return $this->render('demandeur/demandeurPage.html.twig', [
            'controller_name' => 'DemandeurController',
        ]);
    }

    /**
     * Controlleur pour la liste des demandes du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/afficheListeDemandeDemandeur", name="afficheListeDemandeDemandeur")
     */  
    public function afficheListeDemandeDemandeur(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Demande::class);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $donnees = $repository->findAllDemandesDemandeur($user->getid());

        if (!$donnees) {
            $this->addFlash('pb', "pas de demande");
        }

        $listeDemande = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('demandeur/afficheListeDemande.html.twig', array(
                'listeDemande' => $listeDemande,
                'nbPages' => ceil($listeDemande->getTotalItemCount()/7))
        );
    }

    /**
     * Controlleur de création de demandes
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/formulaireCreationDemande", name="formulaireCreationDemande")
     */
    public function formulaireCreationDemande(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Unite::class);
        $listeUnite = $repositoryU->findAllUnite();
        if (!$listeUnite) {
            throw $this->createNotFoundException(
                'pas d\'unite'
            );
        }
        $repositoryA=  $entityManager->getRepository(Article::class);
        $listeArticle = $repositoryA->findAllArticle();
        if (!$listeArticle) {
            throw $this->createNotFoundException(
                'pas d\'article'
            );
        }
        $repositoryE=  $entityManager->getRepository(Zone::class);
        $listeEntrepotS = $repositoryE->findAllEntrepot();
        $listeEntrepotD = $repositoryE->findAllEntrepot();
        if (!$listeEntrepotD) {
            throw $this->createNotFoundException(
                'pas d\'entrepot'
            );
        }
        $dateDemain = new \DateTime();
        $dateDemain->add(new \DateInterval('P1D'));
        $dateDemain = date_format($dateDemain, 'Y-m-d');

        return $this->render('demandeur/AjouterDemande.html.twig', [
            'listeUnite' => $listeUnite,
            'listeArticle' => $listeArticle,
            'listeEntrepotS' => $listeEntrepotS,
            'listeEntrepotD' => $listeEntrepotD,
            'dateDemain' => $dateDemain
        ]);
    }

    /**
     * Controlleur d'ajout de demandes
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/AjouterDemande", name="AjouterDemande")
     */  
    public function AjouterDemande(Request $request):Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $demande = new Demande();

        if ($request->isMethod('POST')) {
            if ($this->verificationCreationDemande(
                $request->request->get("form")['dateBesoin'],
                $request->request->get("form")['entrepotS'],
                $request->request->get("form")['entrepotD'],
                $request->request->get("form")['article'],
                $request->request->get("form")['quantite'],
                $request->request->get("form")['unite']
            )) {
                $user = $this->get('security.token_storage')->getToken()->getUser();
                $demande->setIdDemandeur($user);
                $demande->setEtat(0);
                $demande->setDateCreation(new \DateTime());
                $dateBesoin = $request->request->get("form")['dateBesoin'];
                $demande->setDateBesoin(new \DateTime($dateBesoin));
                $entrepotS = $entityManager->getRepository(Zone::class)->find($request->request->get("form")['entrepotS']);
                $demande->setEntrepotSource($entrepotS);
                $entrepotD = $entityManager->getRepository(Zone::class)->find($request->request->get("form")['entrepotD']);
                $demande->setEntrepotDest($entrepotD);
                $article =  $entityManager->getRepository(Article::class)->find($request->request->get("form")['article']);
                $demande->setArticle($article);
                $quantite = $request->request->get("form")['quantite'];
                $demande->setQuantite($quantite);
                $unite =  $entityManager->getRepository(Unite::class)->find($request->request->get("form")['unite']);
                $demande->setUnite($unite);
                $entityManager->persist($demande);
                $entityManager->flush();
                $this->addFlash('success', 'la demande a bien été créée');
                return $this->redirect($this->generateUrl("afficheListeDemandeDemandeur"));
            }
        }
        return $this->redirect($this->generateUrl("formulaireCreationDemande"));
    }

    /**
     * Controlleur de vérification de création de demandes
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     */  
    public function verificationCreationDemande(String $dateBesoin, String $idES, String $idED, String $idArticle, String $quantite, String $idUnite): bool
    {
        $estValide = true;
        if ($dateBesoin == "" or $idES == "" or $idED == "" or $idArticle == "" or $quantite == "" or $idUnite == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }
        $repositoryU= $this->getDoctrine()->getRepository(Unite::class);
        $profile = $repositoryU->find($idUnite);

        if (!$profile) {
            $this->addFlash('erreur', "l'unité selectionné n'existe pas");
            $estValide = false;
        }

        $repositoryE= $this->getDoctrine()->getRepository(Zone::class);
        $EntrepotD = $repositoryE->find($idED);
        $EntrepotS = $repositoryE->find($idES);

        if (!$EntrepotS or !$EntrepotD) {
            $this->addFlash('erreur', "l'entrepot selectionné n'existe pas");
            $estValide = false;
        }

        $repositoryA= $this->getDoctrine()->getRepository(Article::class);
        $article = $repositoryA->find($idArticle);

        if (!$article) {
            $this->addFlash('erreur', "l'article selectionné n'existe pas");
            $estValide = false;
        }
        $ajd = new \DateTime();
        if ($ajd >= (new \DateTime($dateBesoin))) {
            $this->addFlash('erreur', "la date selectionné est antérieur à aujourd'hui");
            $estValide = false;
        }
        if ($quantite<=0) {
            $this->addFlash('erreur', "la quantité selectionnée est inférieur ou égale à 0");
            $estValide = false;
        }
        if ($idED == $idES) {
            $this->addFlash('erreur', "l'entrepot source est le même que l'entrepot destination");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur d'affichage du profile du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/afficheProfileDemandeur", name="afficheProfileDemandeur")
     */  
    public function afficheProfileDemandeur(): Response
    {
        $user = $this->getUser();
        return $this->render('demandeur/afficheProfile.html.twig', ['utilisateur'=> $user]);
    }

    /**
     * Controlleur de modification du profile du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/modificationProfileDemandeur", name="modificationProfileDemandeur")
     */  
    public function modificationProfileDemandeur(Request $request): Response
    {   
        $utilisateur = $this->getUser();
        $idUtilisateur = $utilisateur->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifUtilisateur(
                $utilisateur->getLogin(),
                $request->request->get("form")['login'],
                $request->request->get("form")['nom'],
                $request->request->get("form")['prenom'],
                $request->request->get("form")['mail'],
                $utilisateur->getEmail()
            )) {
                $login = $request->request->get("form")['login'];
                $nom = $request->request->get("form")['nom'];
                $prenom = $request->request->get("form")['prenom'];
                $utilisateur->setLogin($login);
                $utilisateur->setNom($nom);
                $utilisateur->setPrenom($prenom);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'votre profile a bien été modifié');
                return $this->redirect($this->generateUrl("afficheProfileDemandeur"));
            }
        }
        return $this->render("demandeur/modificationProfile.html.twig", [
            'utilisateur' => $utilisateur
        ]);
    }

    /**
     * Controlleur de modification du mot de passe du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     * @Route("/dem/modificationPasswordDemandeur", name="modificationPasswordDemandeur")
     */  
    public function modificationPasswordDemandeur(UserPasswordEncoderInterface $encoder,Request $request): Response
    {   
        $utilisateur = $this->getUser();
        $idUtilisateur = $utilisateur->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repositoryU=  $entityManager->getRepository(Utilisateur::class);
        $utilisateur = $repositoryU->find($idUtilisateur);

        if (!$utilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur pour l\' id : '.$id
            );
        }

        if ($request->isMethod('POST')) {
            if ($this->verificationModifPassword(
                $utilisateur->getLogin(),
                $request->request->get("form")['motdepasse'],
                $request->request->get("form")['oldmotdepasse'],
                $request->request->get("form")['repeatmotdepasse']
            )) {
                $password = $request->request->get("form")['motdepasse'];
                $encoded = $encoder->encodePassword($utilisateur, $password);
                $utilisateur->setPassword($encoded);

                $entityManager->persist($utilisateur);
                $entityManager->flush();
                $this->addFlash('success', 'votre mot de passe a bien été modifié');
                return $this->redirect($this->generateUrl("afficheProfileDemandeur"));
            }
        }
        return $this->render("demandeur/modificationPassword.html.twig", [
            'utilisateur' => $utilisateur
        ]);
    }

    /**
     * Controlleur de verification de modification du mot de passe du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     */
    public function verificationModifPassword(String $login, String $password, String $oldpassword): bool
    {
        $estValide = true;
        if (!password_verify($oldpassword,$this->getUser()->getPassword())) {
            $this->addFlash('erreur', "ancien mot de passe incorrect");
            $estValide = false;
        }    
        if (strlen($password) > 255 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
         if ($password !== $repeatpassword) {
            $this->addFlash('erreur', "les nouveaux mots de passe sont differents");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur vérification de modification du profile du demandeur
     *
     * Require ROLE_DEMANDEUR for only this controller method.
     * 
     * @IsGranted("ROLE_DEMANDEUR")
     * 
     */
    public function verificationModifUtilisateur(String $oldLogin, String $login, String $nom, String $prenom, String $mail, String $oldMail): bool
    {
        $estValide = true;
        if ($login == "" or $nom == "" or $prenom == "" ) {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }

        $repository= $this->getDoctrine()->getRepository(Utilisateur::class);
        $listeUtilisateur = $repository->findAllUtilisateur();

        if (!$listeUtilisateur) {
            throw $this->createNotFoundException(
                'pas d\'utilisateur'
            );
        }
        $c=0;
        $c2=0;
        for ($i=0; $i<count($listeUtilisateur) ; $i++) { 
            if (strcmp($login,$listeUtilisateur[$i]['login']) == 0) {
                $c = $c+1;
            }
            if (strcmp($mail,$listeUtilisateur[$i]['mail']) == 0) {
                $c2= $c2+1;
            }
        }
        if ($login !== $oldLogin) {
            if ($c !== 0) {
                $this->addFlash('erreur', "le login existe déjà");
            $estValide = false;
            }
        }
        if ($mail !== $oldMail) {
            if ($c2 !== 0) {
                $this->addFlash('erreur', "l'adresse mail existe déjà");
            $estValide = false;
            }
        }
        
        if (strlen($login) >50 or strlen($nom) > 255 or strlen($prenom) > 255 or strlen($mail) > 255 ) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    public function index(): Response
    {
        return $this->render('demandeur/index.html.twig', [
            'controller_name' => 'DemandeurController',
        ]);
    }
}