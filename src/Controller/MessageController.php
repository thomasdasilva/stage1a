<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Entity\Message;
use App\Form\CreationMessageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;

class MessageController extends AbstractController
{
    /**
     * Controlleur pour la liste des message
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheListeMessage", name="afficheListeMessage")
     * @return Response Objet contenant le template
     */
    public function afficheListeMessage(Request $request, PaginatorInterface $paginator):Response
    {
        $repository= $this->getDoctrine()->getRepository(Message::class);
        $donnees = $repository->findAllMessages();
        if (!$donnees) {
            $this->addFlash('pb', "pas de message");
        }
        $listeMessage = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('message/afficheListeMessage.html.twig', array(
                'listeMessage' => $listeMessage,
                'nbPages' => ceil($listeMessage->getTotalItemCount()/7))

        );
    }

    /**
     * Controlleur pour la liste des message
     * 
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheListeMessageCariste", name="afficheListeMessageCariste")
     * @return Response Objet contenant le template
     */
    public function afficheListeMessageCariste(Request $request, PaginatorInterface $paginator):Response
    {
        $user = $this->getUser();
        $donnees = $user->getMessages();
        //dump($donnees);die();
        if (!$donnees) {
            $this->addFlash('pb', "pas de message");
        }

        $orderBy = (Criteria::create())->orderBy([
                'dateEnvoie' => Criteria::DESC,
            ]);

        $listeMessage = $paginator->paginate(
            $donnees->matching($orderBy)->toArray(), // Requête contenant les données à paginer (ici nos demandes)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            7, // Nombre de résultats par page
        );

        return $this->render('message/afficheListeMessage.html.twig', array(
                'listeMessage' => $listeMessage,
                'nbPages' => ceil($listeMessage->getTotalItemCount()/7)),
        );
    }

    /**
     * Controlleur pour la création de message
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/creationMessage", name="creationMessage")
     * @return Response Objet contenant le template
     */
    public function creationMessage(Request $request):Response
    {
        $message = new Message();
        $message->setDateEnvoie(new \DateTime('now'));
        $message->setExpediteur($this->getUser());
        $message->setVu(0);
        $form = $this->createForm(CreationMessageType::class, $message);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            
            if ($this->verificationMessage(
                $request->request->get('creation_message')['titre'],
                $request->request->get('creation_message')['destinaire'],
                $request->request->get('creation_message')['message'],
            )){
                $i=0;
                foreach ($request->request->get('creation_message')['destinaire'] as $request->request->get('creation_message')['destinaire']) {
                    $message->addDestinataire($this->getDoctrine()->getManager()->getRepository(Utilisateur::class)->find($request->request->get('creation_message')['destinaire'][$i]));
                    $i=$i+1;
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($message);
                $entityManager->flush();
                $this->addFlash('success', 'le message a été envoyé');
                return $this->redirect($this->generateUrl("afficheListeMessage", ['page' => '1']));
            }
        }
        return $this->render('message/creationMessage.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Controlleur de vérification de message
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @return Response Objet contenant le template
     */
    public function verificationMessage(String $titre, array $destinataire, String $message): bool
    {
        $estValide = true;
        if ($titre == "" or $message == "") {
            $this->addFlash('erreur', "champ vide");
            $estValide = false;
        }
        if (empty($destinataire)) {
            $this->addFlash('erreur', "pas de cariste sélectionné");
            $estValide = false;
        }

        if (strlen($titre) > 255) {
            $this->addFlash('erreur', "dépacement du nombre maximum de caractère");
            $estValide = false;
        }
        return $estValide;
    }

    /**
     * Controlleur pour l'affichage d'un message
     * 
     * Require ROLE_ADMIN for only this controller method.
     * 
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/admin/afficheMessage/{idMessage}", name="afficheMessage")
     * @return Response Objet contenant le template
     */
    public function afficheMessage(Request $request, int $idMessage):Response
    {
        {
        $repository= $this->getDoctrine()->getRepository(Message::class);
        $donnees = $repository->find($idMessage);

        if (!$donnees) {
            $this->addFlash('pb', "pas de message");
        }
        return $this->render('message/afficheMessage.html.twig', array(
                'message' => $donnees
        ));
        }
    }

    /**
     * Controlleur pour l'affichage d'un message
     * 
     * Require ROLE_CARISTE for only this controller method.
     * 
     * @IsGranted("ROLE_CARISTE")
     * 
     * @Route("/cariste/afficheMessageCariste/{idMessage}", name="afficheMessageCariste")
     * @return Response Objet contenant le template
     */
    public function afficheMessageCariste(Request $request, int $idMessage):Response
    {
        //dump($request);die();
        $repository= $this->getDoctrine()->getRepository(Message::class);
        $donnees = $repository->find($idMessage);

        if (!$donnees) {
            $this->addFlash('pb', "pas de message");
        }

        $user = $this->getUser();
        $messagesCariste = $user->getMessages();
        if (!$messagesCariste) {
            $this->addFlash('pb', "le cariste n'a pas de message");
        }
        $c=0;
        foreach ($messagesCariste->getValues() as $key => $value) {
            if ($messagesCariste->getValues()[$key]->getId() == $donnees->getId()) {
                $c = 1;
            }
        }
        if ($c != 1) {
            return $this->redirect($this->generateUrl("afficheListeMessageCariste", ['page' => '1']));
        }
        $entityManager = $this->getDoctrine()->getManager();
        $donnees->setVu(true);
        $entityManager->persist($donnees);
        $entityManager->flush();
        return $this->render('message/afficheMessage.html.twig', array(
                'message' => $donnees
        ));
    }

    /**
     * @Route("/message", name="message")
     */
    public function index(): Response
    {
        return $this->render('message/index.html.twig', [
            'controller_name' => 'MessageController',
        ]);
    }
}
