<?php

namespace App\Form;

use App\Entity\Demande;
use App\Entity\Utilisateur;
use App\Entity\Article;
use App\Entity\Zone;
use App\Entity\Unite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreationDemandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateBesoin')
            ->add('quantite')
            ->add('entrepotSource', EntityType::class, [
                'class' => Zone::class,
                'choice_label' => 'libelle'])
            ->add('entrepotDest', EntityType::class, [
                'class' => Zone::class,
                'choice_label' => 'libelle'])
            ->add('article', EntityType::class, [
                'class' => Article::class,
                'choice_label' => 'libelle'])
            ->add('unite', EntityType::class, [
                'class' => Unite::class,
                'choice_label' => 'libelle'])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Demande::class,
        ]);
    }
}
