Application web from Scratch sous Symfony pendant mon stage de première année de BTS

Entreprise : Axis Solutions
Client : Sanofy

L'application gère des demandes entre des Demandeurs et des Carristes

* Fonctionnement :
    * Les Demandeurs : Creer des demandes
    * Les Carristes : Livre les commandes et valide sur l'application web
    * Les Administrateurs : Attribuer des commandes aux carristes, gèrer le fonctionnement du site, consulter les statistiques

Techno : PHP/Symfony, HTML/CSS, Bootstrap, Doctrine ORM, SASS, Graph.js
